﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Valve.VR.InteractionSystem
{
    public class TudeFrezze : MonoBehaviour
    {
        public Rigidbody tube_r;
        public SteamVR_Action_Boolean TriggerClick;
        private SteamVR_Input_Sources inputSource;
        private Interactable interactable;
        private double P_X;
        private double P_Y;
        private double P_Z;
        private double R_X;
        private double R_Y;
        private double R_Z;
        private Vector3 Pos;
        private Quaternion Rot;
        private bool flag_col;
        // Start is called before the first frame update
        void Start()
        {
            Pos = transform.position;
            Rot = transform.rotation;

            interactable = this.GetComponent<Interactable>();
        }
        private void OnCollisionEnter(Collision collision)
        {
            if (collision.collider.tag == "Phone")
            {
                
                tube_r.constraints = RigidbodyConstraints.FreezeAll;
                transform.position = Pos;
                transform.rotation = Rot;
            }
        }
        private void OnCollisionExit(Collision collision)
        {
            tube_r.constraints = RigidbodyConstraints.None;
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}