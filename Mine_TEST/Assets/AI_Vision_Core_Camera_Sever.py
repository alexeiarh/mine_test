# Система управления дорожкой на основе машинного зрения
#
# используется веб-камера
#
# код определения положения в get_tracker_camera. Используется нейронная сеть objectron и трекинг от opencv
#
# функция управления дорожкой - get_speed
#
# отправка на ардуино и по сокету в юнити присутствует
#
# Параметры для отладки:
# p1 - разность между ногами за 1 кадр
# p2 - длина безопасной зоны
# p3 и p4 - границы для торможения и остановки соответственно (foot_stop)
# p5 - количество кадров между вызовом objectron(нейронная сеть - обнаружение ног)

from datetime import datetime
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5 import uic
import threading
import serial
import cv2
import time, csv
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg, NavigationToolbar2QT as NavigationToolbar
import sys
import socket
import mediapipe as mp
MAX_DELTA = 150
import math


host = 'localhost'
port = 7775
backlog = 5
size = 4
my_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
my_socket.bind((host,port))
my_socket.listen(backlog)


u = 0
SERIAL = None
#UDP_IP = str(socket.gethostbyname(socket.gethostname()))
drag_coefficient = 255
max_speed = 255
UDP_PORT_Rec = 1755
UDP_PORT_Angle = 1755
UDP_PORT_Unity = 1755
UDP_PORT_Rec_Mine = 3040
UDP_PORT_Angle_Mine = 3040
UDP_PORT_Unity_Mine = 3031
open_window = False

# class Server:
#     def __init__(self):
#         print("...Connected...")
#         server_thread = threading.Thread(target=self.server_threading())
#         server_thread.start()
#
#     def server_threading(self):
#         print("Connected...")
#         client, address = my_socket.accept()
#         print("Connected")
#         while True:
#             data = client.recv(size)
#             print("server = ", str(data))
#             if str(data) == "b'OK'":
#                 # client.send(mess.to_bytes(8,byteorder="big",signed = True))
#                 # client.send(struct.pack(">q",mess))
#                 client.send(bytes(str("self.current_speed"), 'utf-8'))  # работает в UTF-8
#                 # client.send(b'coll') #работает в UTF-8
#             else:
#                 print("Server lost")
#                 break
#
# g1 =  Server()
class PLTWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self._main = QWidget()
        self.max_y = 0
        self.setCentralWidget(self._main)
        layout = QVBoxLayout(self._main)
        self.controll_clear = 0

        dynamic_canvas = FigureCanvasQTAgg(Figure(figsize=(5, 3)))
        layout.addWidget(dynamic_canvas)
        self.addToolBar(Qt.BottomToolBarArea,
                        NavigationToolbar(dynamic_canvas, self))

        self._dynamic_ax = dynamic_canvas.figure.subplots()
        self.x = []
        self.y = []
        self.y2 = []
        self._line, = self._dynamic_ax.plot(self.x, self.y)
        self._line2, = self._dynamic_ax.plot(self.x, self.y2)
        self._timer = dynamic_canvas.new_timer(30)
        self._timer.add_callback(self.update_graph)
        self._timer.start()

    def update_graph(self):
        self._dynamic_ax.clear()
        self._dynamic_ax.plot(self.x, self.y)
        self._dynamic_ax.plot(self.x, self.y2)
        self._line.figure.canvas.draw()
        self._line2.figure.canvas.draw()

    def closeEvent(self, event):
        global open_window
        open_window = False

class TreadmillControl(QMainWindow):
    def __init__(self):
        #server

        super(QMainWindow, self).__init__()
        uic.loadUi('ui_new5.ui', self)
        self.setWindowTitle('Treadmill')
        self.current_speed = 0
        self.default_speed = 0
        self.max_y = 0
        self.min_y = 0
        self.y_shoes = []
        self.max_speed = 255
        self.angle = 0
        self.ErrorConnectionSoket = False
        self.ErrorConnectionSoket_Mine = False
        self.foot_stop = 0
        self.data_coord = [[0,0]]*300
        self.current_y = 0

        self.graph_active = False

        self.MainWhile = False
        self.ArdWhile = False
        self.run_camera = False
        # forest
        #self.server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        #self.server.bind((UDP_IP, UDP_PORT_Angle))

        # mine
        #self.server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        #self.server.bind((UDP_IP, UDP_PORT_Angle))




        # Предустановка Arduino
        try:
            x = self.Search()
            self.COM_port = x[0]
            self.ArdComPort.setText(f'''<p align="center">{x[0]}</p>''')
        except:
            self.COM_port = None

        self.ard_speed = 115200
        self.arduino = None
        if self.arduino:
            try:
                self.ard_connect()
            except:
                pass

        self.action = 0
        self.PLTWindow = None

        self.record_flag = True
        self.speed_unity_k = 2

        # Ui
        self.StartButton.clicked.connect(self.start)
        self.Angle1.clicked.connect(self.angle_1)
        self.Angle2.clicked.connect(self.angle_2)
        self.Angle_Stop.clicked.connect(self.angle_stop)
        self.UP_Button.clicked.connect(self.update_ip)

        self.Graph_button.clicked.connect(self.graph)
        self.StartCameraButton.clicked.connect(self.camera_thread2)
        self.StopButton.clicked.connect(self.stop)
        self.IP.setText(str(socket.gethostbyname(socket.gethostname())))
        self.spinBoxk1.setValue(0.001)
        self.spinBoxk2.setValue(0.001)
        self.spinBoxk1.setSingleStep(0.001)
        self.spinBoxk2.setSingleStep(0.001)
        self.p1.setValue(0.02)
        self.p2.setValue(0.2)
        self.p3.setValue(0.15)
        self.p4.setValue(0.25)
        self.p5.setValue(5)
        self.p1.setSingleStep(0.01)
        self.p2.setSingleStep(0.05)
        self.p3.setSingleStep(0.05)
        self.p4.setSingleStep(0.05)

        #   -- Ard control
        self.Connect.clicked.connect(self.ard_connect)
        self.Disconnect.clicked.connect(self.ard_disconnect)
        #   -- Ard settings
        self.ArdComPortSelect.clicked.connect(self.ard_change_port)
        self.num_point = 0





    def camera_thread2(self):

        camera_thread = threading.Thread(target=self.get_speed_camera)
        camera_thread.start()

    def angle_1(self):
        self.angle = 1
        print("COMMAND1   " + str(int(self.current_speed)) + ',  ' + str(int(self.angle)) + '.')

    def angle_2(self):
        self.angle = 2
        print("COMMAND2   " + str(int(self.current_speed)) + ',  ' + str(int(self.angle)) + '.')

    def angle_stop(self):
        self.angle = 0
        print("COMMAND2   " + str(int(self.current_speed)) + ',  ' + str(int(self.angle)) + '.')

    def graph(self):
        global open_window
        open_window = True
        self.PLTWindow = PLTWindow()
        self.PLTWindow.show()
        graph_while_thread = threading.Thread(target=self.graph_while)
        graph_while_thread.start()

    def graph_while(self):

        if self.PLTWindow:
            print("STARTTTTT")
            NUM = min(20, len(self.data_coord))
            if len(self.data_coord) > 303:
                self.PLTWindow.x = [self.num_point - 300 + i for i in range(300)]

                self.PLTWindow.y = [i[0] for i in self.data_coord[-301:-1]]
                self.PLTWindow.y2 = [i[1] for i in self.data_coord[-301:-1]]
            else:
                self.PLTWindow.x = [0 for i in range(300)]

                self.PLTWindow.y = [0 for i in range(300)]
                self.PLTWindow.y2 = [0 for i in range(300)]

            while open_window:
                try:
                    # self.PLTWindow.controll_clear += 1
                    self.PLTWindow.x = self.PLTWindow.x[1:]
                    self.PLTWindow.x.append(self.num_point)
                    self.PLTWindow.y = self.PLTWindow.y[1:]
                    self.PLTWindow.y.append(self.data_coord[-1][0])
                    self.PLTWindow.y2 = self.PLTWindow.y2[1:]
                    self.PLTWindow.y2.append(self.data_coord[-1][1])
                    # self.PLTWindow.y = [5 for i in range(10)]
                    self.num_point += 1
                    time.sleep(0.033)

                except Exception:
                    print("Error canvas")
                    return
        print("exit")
        return

    def closeEvent(self, event):
        print("EXITING")
        if self.arduino != None:
            self.stop()
            print("EXIT")

    def start(self):
       # self.ErrorConnectionSoket = False
       # self.ErrorConnectionSoket_Mine = False



        # try:
        #     #mine
        #     self.conn_mine = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        #     self.conn_mine.bind(('', UDP_PORT_Rec_Mine))
        # except ConnectionRefusedError:
        #     print("Create socket error!")
        #     self.ErrorConnectionSoket_Mine = True
        #
        # try:
        #     # forest
        #     self.conn = socket.socket()
        #     self.conn.connect((UDP_IP, UDP_PORT_Unity))
        # except ConnectionRefusedError:
        #     self.ErrorConnectionSoket = True

        self.calibration_zone = True
        if not self.arduino:
            self.console_output("Соединение с Ардуино не установлено.", color="#f80000")
            print(self.arduino)
            print("Not connection with arduino")
        else:
            print(self.arduino)
            self.run_camera = True
            self.arduino.write(bytes(str("Treadmill") + '.', 'utf-8'))
            time.sleep(0.05)
            answer = self.arduino.readline()
            print(answer)
            attempt = 0
            while attempt < 50:
                attempt += 1
                self.arduino.write(bytes(str("Treadmill") + '.', 'utf-8'))
                time.sleep(0.05)
                answer = self.arduino.readline()
                print(answer)
                a1 = "Speed".encode() in answer
                if a1:
                    break

            if attempt >= 50:
                self.console_output("Проблема с подключение к СОМ. Переподключитесь заново.", color="#f80000")
                self.Connect.setEnabled(True)
                self.Disconnect.setEnabled(False)
                self.arduino = None
            else:
                print("**************************")
                print(self.arduino.readline())
                print("**************************")

                self.console_output("Платформа запущена.", color="#0000f8")
                # self.MainWhile = True
                camera_thread = threading.Thread(target=self.get_tracker_camera)
                camera_thread.start()

                self.StartButton.setEnabled(False)
                self.ArduinoBar.setEnabled(False)
                self.StopButton.setEnabled(True)

    def get_tracker_camera(self):
        #Основная функция определения положения
        mp_objectron = mp.solutions.objectron
        objectron = mp_objectron.Objectron(static_image_mode=False,
                                           max_num_objects=2,
                                           min_detection_confidence=0.4,
                                           min_tracking_confidence=0.90,
                                           model_name='Shoe')

        # Read video
        video = cv2.VideoCapture(0)
        video.set(cv2.cv2.CAP_PROP_AUTOFOCUS, 0)
        # 70 - focus on treadmill
        video.set(cv2.cv2.CAP_PROP_FOCUS, 70)
        video.set(cv2.cv2.CAP_PROP_FRAME_WIDTH, 1920)
        video.set(cv2.cv2.CAP_PROP_FRAME_HEIGHT, 1080)
        # Exit if video not opened.
        if not video.isOpened():
            print("Could not open video")
            self.console_output("Не удалось подключиться к камере", color="#ff0000")
            while True:
                video = cv2.VideoCapture(1)
                if video.isOpened():
                    break
        # Read first frame.
        ok, frame = video.read()
        if not ok:
            print('Cannot read video file')
            sys.exit()
        # Uncomment the line below to select a different bounding box
        xs = frame.shape[1]
        ys = frame.shape[0]
        mas_item = []
        trackers = []
        not_foot_lost = 0
        last_y = 0
        self.current_y = 0
        search = True
        frame_count = 0
        print("detecting ...")
        while search:
            ok, frame = video.read()

            results = objectron.process(frame)

            if results.detected_objects:
                print(len(results.detected_objects))
                for detected_object in results.detected_objects:
                    x = [i.x for i in detected_object.landmarks_2d.landmark]
                    y = [i.y for i in detected_object.landmarks_2d.landmark]
                    min_x = min(x)
                    max_x = max(x)
                    min_y = min(y)
                    max_y = max(y)

                    mas_item.append((int(xs * min_x),
                                     int(ys * min_y),
                                     abs(int(xs * (max_x - min_x))),
                                     abs(int(ys * (max_y - min_y)))))
                    trackers.append(cv2.TrackerKCF_create())

                for t in range(len(trackers)):
                    trackers[t].init(frame, mas_item[t])

                search = False
        print("detecting ...ok")

        while True:
            print("connect ...")
            client, address = my_socket.accept()
            print("connect ...ok")
            while self.run_camera:

                try:
                    # Read a new frame

                    delta_stop = self.p1.value()
                    # tr_len = self.p3.value()

                    status, frame = video.read()
                    frame_count += 1
                    if not status:
                        print("EXCEPTION: THE CAMERA IS LOST")
                        print(self.arduino, self.ArdWhile)
                        self.last_speed = 0
                        self.current_speed = 0

                        self.StartButton.setEnabled(True)
                        if self.arduino:
                            self.ExtremeStop()

                    # Update tracker
                    foot = 0
                    mas_item = []
                    for t in trackers:

                        ok, bbox = t.update(frame)
                        # Draw bounding box
                        if ok:
                            foot += 1
                            p1 = (int(bbox[0]), int(bbox[1]))
                            p2 = (int(bbox[0] + bbox[2]), int(bbox[1] + bbox[3]))
                            cv2.rectangle(frame, p1, p2, (255, 0, 0), 2, 1)
                            mas_item.append(bbox)
                    # 5 in second
                    if foot < 2: #or frame_count % 1 == 0:
                        print(" ")
                        print("WARNING")
                        print(" ")
                        mas_item = []
                        trackers = []
                        results = objectron.process(frame)
                        if results.detected_objects:
                            not_foot_lost = 0
                            for detected_object in results.detected_objects:
                                x = [i.x for i in detected_object.landmarks_2d.landmark]
                                y = [i.y for i in detected_object.landmarks_2d.landmark]
                                min_x = min(x)
                                max_x = max(x)
                                min_y = min(y)
                                max_y = max(y)

                                mas_item.append((max(int(xs * min_x), 0),
                                                 max(int(ys * min_y), 0),
                                                 abs(int(xs * (max_x - min_x))) + 1,
                                                 abs(int(ys * (max_y - min_y))) + 1))
                                trackers.append(cv2.TrackerKCF_create())
                            # print(mas_item)
                            if len(trackers) > 0:
                                for t in range(len(trackers)):
                                    trackers[t].init(frame, mas_item[t])
                        else:
                            not_foot_lost += 1

                    if len(mas_item) >= 1:
                        last_y = self.current_y
                        # print("MASS", mas_item)
                        self.current_y = (sum([i[1] + i[3] for i in mas_item]) / len(mas_item)) / ys

                    if abs(self.current_y - last_y) <= delta_stop:
                        self.foot_stop += 1
                    else:
                        self.foot_stop = 0
                    self.last_speed = self.current_speed
                    if not_foot_lost <= 2 and self.foot_stop <= 2:
                        # self.current_speed = self.get_speed_camera_foot_v2(self.current_y)
                        self.current_speed = self.get_speed(self.current_y)
                    elif 3 <= not_foot_lost <= 6 or self.foot_stop > 2:
                        # ТОРМОЖЕНИЕ
                        self.current_speed = self.current_speed / 2
                    elif not_foot_lost > 6 or self.foot_stop > 10:
                        self.current_speed = 0
                    # cv2.imwrite("C:\\frame\{0}.png".format(str(time.time())), frame)
                    print(abs(self.current_y - last_y), "\t", not_foot_lost, "\t", self.foot_stop, "\t", self.current_y,
                          "\t", self.current_speed)
                    self.data_coord.append([self.current_y, self.current_speed])

                    self.arduino.write(bytes(str(int(self.current_speed)) + ',' + str(int(self.angle)) + '.', 'utf-8'))
                    # Нельзя удалять чтение данных с Arduino, иначе программа будет сильно тормозить
                    m = self.arduino.readline().decode()

                    # #Отправка на unity
                    # data = client.recv(size)
                    # print("server = ", str(data))
                    # if str(data) == "b'OK'":
                    #     client.send(bytes(str(self.current_speed), 'utf-8'))  # работает в UTF-8
                    # else:
                    #     print("Server lost")
                    #     break




                    video_speed = (self.current_speed+self.last_speed)/2
                    # if not self.ErrorConnectionSoket:
                    #     try:
                    #         self.conn.send(
                    #             bytes(str(int(abs(video_speed//30))), 'utf-8'))
                    #     except Exception as e:
                    #         self.ErrorConnectionSoket = True

                    # if not self.ErrorConnectionSoket_Mine:
                    #     try:
                    #         self.conn_mine.sendto(
                    #             bytes(str(int(video_speed * self.speed_unity_k)).rjust(4, " "), 'utf-8'),
                    #             (UDP_IP, UDP_PORT_Unity_Mine))
                    #     except Exception as e:
                    #         print("Send socket error!")
                    #         self.ErrorConnectionSoket_Mine = True
                    # else:
                    #     print("Send socket error!")
                    #     print(video_speed,  self.conn_mine)

                    self.Display.display(int(self.current_speed))
                except Exception as e:
                    print("MAIN EXCEPTION", e, e.__class__)
                    print(self.arduino, self.ArdWhile)
                    self.current_speed = 0

                    self.StartButton.setEnabled(True)
                    if self.arduino:
                        self.ExtremeStop()
                    return
                # Отправка на unity
                try:
                    data = client.recv(size)
                    print("server = ", str(data))
                    if str(data) == "b'OK'":
                        client.send(bytes(str(int(self.current_speed)), 'utf-8'))  # работает в UTF-8
                    else:
                        print("Server lost")
                        break
                except Exception as e:
                    print("Server lost")
                    break


    def get_speed_camera_foot_v3(self, y, y_prev):
        k1 = self.spinBoxk1.value()
        k2 = self.spinBoxk2.value()
        delta_time = time.time() - self.time_moving
        speed = (y - y_prev)/delta_time
        self.time_moving = time.time()
        delta_speed = k1 * (y_prev - y) + k2 * speed

        return speed + delta_speed


    def get_speed_camera_foot_v2(self, y):
        # функция управления скоростью
        t1 = 0.35
        t2 = 0.45
        t3 = 0.65
        t4 = 0.9
        s = abs(self.current_speed)
        # if self.foot_stop > 8:
        #     return 0
        # if self.foot_stop > 4:
        #     self.default_speed = self.default_speed/2
        if y < t1:
            s = 0
            self.default_speed = 0
        elif t1 < y < t2:
            s = (y-t1)/(t2-t1)*self.default_speed
        elif t2 < y < t3:
            s = self.default_speed
        elif y > t3:
            s = (20 * s + (255 - s) * (y - t3) / (t4 - t3)) / 20
            self.default_speed = s

        return s * (-1)

    def get_speed_camera_foot_v1(self, y):
        #Первая функция для расчета скорости
        top = 0.2
        bottom = 0.9
        center = 0.4
        s = abs(self.current_speed)
        if self.foot_stop:
            print("alarm!!!")
        if y < top:
            s = 0
            #print("alarm!!!")
        elif top < y < center:
            s = s - 0.01 * s * (center-y) / (center-top)
        elif y > center:
            s = s + (255 - s) * 0.01 * (y - center) / (bottom - center) / 1.5

        #print("y =", y)
        return s * (-1)

    def get_speed_foot_camera_v4(self, y):
        #функция управления скорости
        top = 0.35
        center = 0.7
        bottom = 1.0
        s = abs(self.current_speed)

        if y < top:
            s = 0
        elif s <= 50 and top < y <= center:
            s = math.pow(y, 0.6) * 65
        elif s > 50:
            if center < y < bottom:
                s += 1
            elif y < top:
                s -= 20
        s = max(s,0)
        s = min(s, 255)
        return s * (-1)

    def get_speed(self, z):
        safe_zona = self.p2.value()
        max_speed = self.max_speed
        tr_len = 0.9
        #safe_zona = 0.2
        zn = -1
        z = abs(z)
        if z < safe_zona:
            # print("safe zona")
            return 0
        elif safe_zona <= z <= tr_len:
            delta = tr_len - safe_zona
            if z * max_speed <= max_speed:
                speed = (z - safe_zona) * max_speed / (delta)
                if speed < 30:
                    speed = 30

                # print("work zona")
                return zn * min(max_speed, speed)
            else:

                # print("far zona speed")
                return zn * max_speed
        elif z > tr_len:
            # print("far zona")
            return zn * max_speed
        else:
            print("error")
            return 0

    def get_arduino_speed(self):
        answer = self.arduino.readline().decode()
        return answer

    def ExtremeStop(self):  # problem
        try:
            self.angle = 0
            self.arduino.write(bytes(str(int(self.current_speed)) + ',' + str(int(0)) + '.', 'utf-8'))
            self.console_output("Остановка платформы.", color="#f80000")
            print("*" * 10, "Extreme stop", self.current_speed)
            self.MainWhile = False
            self.run_camera = False

            if self.current_speed > 0:
                time.sleep(0.1)
                self.arduino.write(bytes(str('Disconnect') + '.', 'utf-8'))
                time.sleep(0.05)
                answer = self.arduino.readline().decode()
                print(answer)
                while self.current_speed > 0 and "Wait" not in answer:
                    if self.arduino:
                        self.current_speed -= 1
                        time.sleep(0.05)
                        self.arduino.write(bytes(str('Disconnect') + '.', 'utf-8'))
                        # self.conn.sendto(bytes(str(int(self.current_speed*self.speed_unity_k )).rjust(4, " "), 'utf-8'),
                        #                  (UDP_IP, UDP_PORT_Unity))

                        if not self.ErrorConnectionSoket:
                            try:
                                self.conn.send(
                                    bytes(str(int(abs(self.current_speed // 30))), 'utf-8'))
                            except Exception as e:
                                self.ErrorConnectionSoket = True

                        if not self.ErrorConnectionSoket_Mine:
                            try:
                                self.conn_mine.sendto(
                                    bytes(str(int(self.current_speed * self.speed_unity_k)).rjust(4, " "), 'utf-8'),
                                    (UDP_IP, UDP_PORT_Unity))
                            except Exception as e:
                                self.ErrorConnectionSoket_Mine = True

                        m = self.arduino.readline().decode()
                        # print(m)


                    else:
                        break

                if not self.ErrorConnectionSoket_Mine:
                    self.conn_mine.close()
                if not self.ErrorConnectionSoket:
                    self.conn.close()
            else:
                time.sleep(0.1)
                self.arduino.write(bytes(str('-Disconnect') + '.', 'utf-8'))
                time.sleep(0.05)
                answer = self.arduino.readline().decode()
                print(answer)
                while self.current_speed < 0 and "Wait" not in answer:
                    if self.arduino:
                        self.current_speed += 1
                        time.sleep(0.05)
                        # print("extreme", self.current_speed)
                        self.arduino.write(bytes(str('-Disconnect') + '.', 'utf-8'))
                        # self.conn.sendto(bytes(str(int(self.current_speed*self.speed_unity_k )).rjust(4, " "), 'utf-8'),
                        #                  (UDP_IP, UDP_PORT_Unity))
                        if not self.ErrorConnectionSoket:
                            try:
                                self.conn.send(
                                    bytes(str(int(abs(self.current_speed // 30))), 'utf-8'))
                            except Exception as e:
                                self.ErrorConnectionSoket = True

                        if not self.ErrorConnectionSoket_Mine:
                            try:
                                self.conn_mine.sendto(
                                    bytes(str(int(self.current_speed * self.speed_unity_k)).rjust(4, " "), 'utf-8'),
                                    (UDP_IP, UDP_PORT_Unity_Mine))
                            except Exception as e:
                                self.ErrorConnectionSoket_Mine = True

                        m = self.arduino.readline().decode()
                        print(m)
                    else:
                        break

            self.last_speed = 0
            # self.arduino.write(bytes(str(int(0)) + '.', 'utf-8'))
            self.current_speed = 0

            if not self.ErrorConnectionSoket_Mine:
                self.conn_mine.close()
            if not self.ErrorConnectionSoket:
                self.conn.close()

            self.StartButton.setEnabled(True)
            print("STOP complete")
        except Exception as e:
            print("EXTREME", e, e.__class__)
            print(self.arduino, self.ArdWhile)
        self.console_output("Платформа остановлена", color="#f89000")

    def NormalStop(self):  # problem
        try:
            self.arduino.write(bytes(str(int(self.current_speed)) + ',' + str(int(0)) + '.', 'utf-8'))
            self.console_output("Остановка платформы.", color="#f80000")
            print("*" * 10, "Normal stop", self.current_speed)
            self.MainWhile = False

            if self.current_speed > 0:
                time.sleep(0.1)
                self.arduino.write(bytes(str(int(self.current_speed)) + ',' + str(int(0)) + '.', 'utf-8'))
                time.sleep(0.05)
                while self.current_speed > 0:
                    if self.arduino:
                        self.current_speed -= 3
                        time.sleep(0.05)
                        self.arduino.write(bytes(str(int(self.current_speed)) + ',' + str(int(0)) + '.', 'utf-8'))
                        # self.conn.sendto(bytes(str(int(self.current_speed*self.speed_unity_k )).rjust(4, " "), 'utf-8'),
                        #                  (UDP_IP, UDP_PORT_Unity))
                        if not self.ErrorConnectionSoket:
                            try:
                                self.conn.send(
                                    bytes(str(int(abs(self.current_speed // 30))), 'utf-8'))
                            except Exception as e:
                                self.ErrorConnectionSoket = True

                        if not self.ErrorConnectionSoket_Mine:
                            try:
                                self.conn_mine.sendto(
                                    bytes(str(int(self.current_speed * self.speed_unity_k)).rjust(4, " "), 'utf-8'),
                                    (UDP_IP, UDP_PORT_Unity))
                            except Exception as e:
                                self.ErrorConnectionSoket_Mine = True

                        m = self.arduino.readline().decode()
                        #print(m)
                        if "Speed=" in m:
                            status1, status2 = m.split(",")

                        self.Status_2.setText(
                            '''<p align="center"><span style="color:#2f8700;">''' + status1 + '''</span></p>''')

                        self.Status_3.setText(
                            '''<p align="center"><span style="color:#2f8700;">''' + status2 + '''</span></p>''')

                    else:
                        break
            else:
                time.sleep(0.1)
                self.arduino.write(bytes(str(int(self.current_speed)) + ',' + str(int(0)) + '.', 'utf-8'))
                time.sleep(0.05)
                while self.current_speed < 0:
                    if self.arduino:
                        self.current_speed += 3
                        time.sleep(0.05)
                        # print("extreme", self.current_speed)
                        self.arduino.write(bytes(str(int(self.current_speed)) + ',' + str(int(0)) + '.', 'utf-8'))
                        # self.conn.sendto(bytes(str(int(self.current_speed*self.speed_unity_k )).rjust(4, " "), 'utf-8'),
                        #                  (UDP_IP, UDP_PORT_Unity))

                        if not self.ErrorConnectionSoket:
                            try:
                                self.conn.send(
                                    bytes(str(int(abs(self.current_speed // 30))), 'utf-8'))
                            except Exception as e:
                                self.ErrorConnectionSoket = True

                        if not self.ErrorConnectionSoket_Mine:
                            try:
                                self.conn_mine.sendto(
                                    bytes(str(int(self.current_speed * self.speed_unity_k)).rjust(4, " "), 'utf-8'),
                                    (UDP_IP, UDP_PORT_Unity_Mine))
                            except Exception as e:
                                self.ErrorConnectionSoket_Mine = True

                        m = self.arduino.readline().decode()
                        # print(m)
                        if "Speed=" in m:
                            status1, status2 = m.split(",")

                        self.Status_2.setText(
                            '''<p align="center"><span style="color:#2f8700;">''' + status1 + '''</span></p>''')

                        self.Status_3.setText(
                            '''<p align="center"><span style="color:#2f8700;">''' + status2 + '''</span></p>''')
                    else:
                        break

            if not self.ErrorConnectionSoket_Mine:
                self.conn_mine.close()
            if not self.ErrorConnectionSoket:
                self.conn.close()
            self.last_speed = 0
            # self.arduino.write(bytes(str(int(0)) + '.', 'utf-8'))
            self.current_speed = 0

            self.MainWhile = True
            print("STOP complete")
        except Exception as e:
            print("NORMAL", e, e.__class__)
            print(self.arduino, self.ArdWhile)
        self.console_output("Платформа остановлена", color="#f89000")

    def update_ip(self):
        global UDP_IP
        UDP_IP = self.IP.toPlainText()
        print("New IP", UDP_IP)

        self.console_output("Установлен IP: " + str(UDP_IP), color="#0000f8")

    def update_speed(self):
        self.speed_unity_k = int(self.Speed_k_unity.toPlainText())
        print("New self.speed_unity_k", self.speed_unity_k)

        self.console_output("Установлен speed: " + str(self.speed_unity_k), color="#0000f8")

    def stop(self):
        if self.arduino:
            self.ExtremeStop()
        if self.record_flag:
            print("WRITING")
            name = "data" + datetime.strftime(datetime.now(), "%Hh%Mm%Ss")
            self.csv_writer(f'{name}.csv', self.data_coord)
            self.data_coord = []
            print("WRITING END")
        self.StopButton.setEnabled(False)
        self.ArduinoBar.setEnabled(True)

    def ard_connect(self):
        try:
            self.arduino = serial.Serial(self.COM_port, self.ard_speed, timeout=0)
            self.arduino.write(bytes('0.', 'utf-8'))
            self.Status.setText('''<p align="center"><span style="color:#2f8700;">Подключено</span></p>''')
            self.Connect.setEnabled(False)
            self.Disconnect.setEnabled(True)
            self.console_output("Соединение с Ардуино установлено.", color="#2f8700")

        except Exception as e:
            if self.COM_port:
                self.console_output("Соединение с Ардуино не установлено. Проверьте COM-порт или скорость.",
                                    color="#f80000")
            else:
                self.console_output("COM-порт не выбран.", color="#f80000")
            print(e)

    def Search(self, __baudrate=115200):
        __COMlist = []
        __COM = ['COM' + str(i) for i in range(2, 100)]

        for _COM in __COM:
            try:
                COMport = (serial.Serial(port=_COM,
                                         baudrate=__baudrate,
                                         parity=serial.PARITY_NONE,
                                         stopbits=serial.STOPBITS_ONE,
                                         bytesize=serial.EIGHTBITS,
                                         timeout=0))
                if COMport:
                    __COMlist.append(_COM)

            except Exception as e:
                pass
        return __COMlist

    def CheckSerialPortMessage(self, __baudrate=115200, __timeSleep=5):
        try:
            for __COM in self.Search():

                port = serial.Serial(__COM, __baudrate)
                time.sleep(__timeSleep)
                large = len(port.readline())
                port.read(large)

                while large > 3:
                    for a in range(__timeSleep):

                        date = port.readline().decode().split()

                        if 'treadmill' in date:
                            self.arduino = port
                            self.arduino.write("treadmill")

        except Exception as e:
            pass

    def ard_disconnect(self):
        self.arduino = None
        self.Status.setText('''<p align="center"><span style="color:#ff0004;">Отключено</span></p>''')
        self.Connect.setEnabled(True)
        self.Disconnect.setEnabled(False)

    def ard_change_port(self):
        x = self.Search()
        if x:
            new, ok = QInputDialog.getItem(self, "Выберите COM-порт", "Доступные COM-Порты", x, 0, False)
            if ok:
                self.COM_port = new
                self.ArdComPort.setText(f'''<p align="center">{new}</p>''')
        else:
            self.console_output("COM-порты не найдены", color="#fcba03")

    def ard_change_speed(self):
        speeds = ['1200', '2400', '4800', '9600', '19200', '38400', '57600', '115200']
        x = speeds.index(str(self.ard_speed))
        new, ok = QInputDialog.getItem(self, "Cкорость", "Доступные скорости", speeds, x, False)
        if ok:
            self.ard_speed = int(new)
            self.ArdSpeed.display(new)

    def console_output(self, info, *, color: str = ""):
        self.ConsoleOutput.append(
            f'''
                <div style="margin: 2px;">
                        <span>[{datetime.strftime(datetime.now(), "%H:%M:%S")}]</span>
                        <span style="color:{color};">  {info}  </span>
                </div>
            ''')
        self.ConsoleOutput.verticalScrollBar().setValue(self.ConsoleOutput.verticalScrollBar().maximum())
        self.ConsoleOutput.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        return

    def csv_writer(self, path, data):  # запись в цсв
        with open(path, "w", newline='') as out_file:
            writer = csv.writer(out_file, delimiter=';')
            for row in data:
                writer.writerow(row)


def log_uncaught_exceptions(ex_cls, ex, tb):
    text = '{}: {}:\n'.format(ex_cls.__name__, ex)
    import traceback
    text += ''.join(traceback.format_tb(tb))
    print(text)
    print(None, 'Error', text)
    quit()

import sys

sys.excepthook = log_uncaught_exceptions

app = QApplication(sys.argv)
ex = TreadmillControl()
ex.show()
sys.exit(app.exec_())
