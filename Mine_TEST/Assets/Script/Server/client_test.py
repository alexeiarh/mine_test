import socket
import threading
import cv2
import mediapipe as mp
import time
# host = 'localhost'
# port = 7775
# backlog = 5
# size = 4
# UDP_IP = str(socket.gethostbyname(socket.gethostname()))
#
# my_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# my_socket.bind((host,port))
# my_socket.listen(backlog)

mess = 1

class main():
    def __init__(self):
        self.max_speed = 255
        self.address = None
        self.client = None

        self.serv_con = False
        host = 'localhost'
        port = 7775
        backlog = 5
        self.my_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.my_socket.bind((host, port))
        self.my_socket.listen(backlog)
        self.unity_connecting = False;


    def connect_unity(self):
        print("connect ...")
        self.client, self.address = self.my_socket.accept()
        self.serv_con = True
        print("connect ...ok")
        self.unity_connecting = True;
    def unity_process(self):
        t = threading.Thread(target=self.connect_unity)
        t.start()

    def camera(self):

        mp_objectron = mp.solutions.objectron
        mp_drawing = mp.solutions.drawing_utils
        objectron = mp_objectron.Objectron(static_image_mode=False,
                                           max_num_objects=2,
                                           min_detection_confidence=0.4,
                                           min_tracking_confidence=0.90,
                                           model_name='Shoe')


        cv2.namedWindow("Video")
        video = cv2.VideoCapture(1)
        if not video.isOpened():
            print("Could not open video")
            self.console_output("Не удалось подключиться к камере", color="#ff0000")
            while True:
                video = cv2.VideoCapture(1)
                if video.isOpened():
                    break
        # Read first frame.

        while True:

            ok, frame = video.read()

            frame.flags.writeable = False
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            results = objectron.process(frame)

            frame.flags.writeable = True
            frame = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
            if results.detected_objects:
                for detected_object in results.detected_objects:
                    mp_drawing.draw_landmarks(
                        frame, detected_object.landmarks_2d, mp_objectron.BOX_CONNECTIONS)
                    mp_drawing.draw_axis(frame, detected_object.rotation,
                                         detected_object.translation)


            cv2.imshow('Video', frame)

            if cv2.waitKey(5) == 27:
                break
        video.release()
        cv2.destroyAllWindows()

    def start(self):
        main_while_thread = threading.Thread(target=self.main_while)
        main_while_thread.start()
        self.unity_process()
        camera_thread = threading.Thread(target=self.camera)
        camera_thread.start()


    def main_while(self):
        self.moving = False
        self.current_speed = 0
        while True:
            try:
                self.current_speed = -100
                if self.serv_con:
                    try:
                        data = self.client.recv(4)
                        # client.send(int.to_bytes(-self.current_speed, 'big'))
                        # client.send((-self.current_speed).encode())

                        if str(data) == "b'OK'":
                            self.client.send(bytes(str(int(-self.current_speed)), 'utf-8'))  # работает в UTF-8
                            # client.send(int.to_bytes(-self.current_speed,'big'))

                        else:
                            print("Server lost")
                            self.serv_con = False

                    except Exception as e:
                        print("Server lost")
                        self.serv_con = False
                else:
                    if self.unity_connecting:
                        self.unity_process()
            except ZeroDivisionError as zero:

                print("ZERO", zero)
                print(self.current_speed)
                continue


ex = main()
ex.start()
# print("begin...")
# while True:
#     client, address = my_socket.accept()
#     print("Connected")
#     while True:
#         data = client.recv(size)
#         print("server = ", str(data))
#         if str(data) == "b'OK'":
#             #client.send(mess.to_bytes(8,byteorder="big",signed = True))
#             #client.send(struct.pack(">q",mess))
#             client.send(bytes(str(mess),'utf-8')) #работает в UTF-8
#             #client.send(b'coll') #работает в UTF-8
#         else:
#             print("Server lost")
#             break
#         mess+=1
#         if (mess==100):
#             mess = 1