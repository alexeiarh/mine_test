﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
namespace Valve.VR.InteractionSystem
{
    public class grenade : MonoBehaviour
    {

        public SteamVR_Action_Boolean TriggerClick;
        private SteamVR_Input_Sources inputSource;
        public ParticleSystem pena;
        private Interactable interactable;
        public int charge = 500;
        private bool flag_pena_pres = false;
        

        private void Start()
        {
            interactable = this.GetComponent<Interactable>();
            pena.Stop();
        }
        private void Update()
        {
            if(flag_pena_pres && charge > 0)
            {
                charge -= 1;
            }
            if( charge <= 0)
            {
                pena.Stop();
                flag_pena_pres = false;
            }
        }
        private void OnEnable()
        {
            TriggerClick.AddOnStateDownListener(Press, inputSource);
            TriggerClick.AddOnStateUpListener(DePress, inputSource);
        }
        private void OnDisable()
        {
            TriggerClick.RemoveOnStateDownListener(Press, inputSource);
            TriggerClick.RemoveOnStateDownListener(DePress, inputSource);
        }


        private void Press(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
        {
            if(interactable != null && interactable.attachedToHand != null && charge > 0)
            {
                pena.Play();
                flag_pena_pres = true;
            }
        }
        private void DePress(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
        {
            pena.Stop();
            flag_pena_pres = false;
        }

    }
}