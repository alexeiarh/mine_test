﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour
{

    public int HP = 1000;
    private bool flag_on_pena = false;
    public ParticleSystem fire_emission;
    public ParticleSystem fire_emission_2;
    public ParticleSystem fire_emission_3;
    public ParticleSystem fire_emission_4;
    public ParticleSystem fire_emission_5;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnParticleCollision(GameObject other)
    {
        Debug.Log("collisin");
        if (other.tag == "Pena" && HP >= 0)
        {
            Debug.Log("Цель тушиться");
            HP -= 2;
            fire_emission_2.emissionRate -= 0.04f;
            fire_emission_3.emissionRate -= 0.002f;
            fire_emission_4.emissionRate -= 0.02f;
            fire_emission_5.emissionRate -= 0.1f;
        }

    }
    // Update is called once per frame
    void Update()
    {
        if (HP > 0 && HP <1000) {
            HP += 1;
            fire_emission_2.emissionRate += 0.02f;
            fire_emission_3.emissionRate += 0.001f;
            fire_emission_4.emissionRate += 0.01f;
            fire_emission_5.emissionRate += 0.05f;
        }
        if (HP <= 0)
        {
            
            fire_emission.Stop();
            //Destroy(gameObject);
        }
    }
}
