﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class granate_player : MonoBehaviour
{
    // Start is called before the first frame update
    public ParticleSystem granate_emission;
    public ParticleSystem granate_emission_2;
    void Start()
    {
        granate_emission.Stop();
        granate_emission_2.Stop();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.E))
        {
            granate_emission.Play();
            granate_emission_2.Play();
        }
        else
        {
            granate_emission.Stop();
            granate_emission_2.Stop();
        }
    }
}
