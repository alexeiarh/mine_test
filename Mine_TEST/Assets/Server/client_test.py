import socket


host = 'localhost'
port = 7777
backlog = 5
size = 4
my_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
my_socket.bind((host,port))
my_socket.listen(backlog)

mess = 1

print("begin...")
while True:
    client, address = my_socket.accept()
    print("Connected")
    while True:
        data = client.recv(size)
        print("server = ", str(data))
        if str(data) == "b'OK'":
            #client.send(mess.to_bytes(8,byteorder="big",signed = True))
            #client.send(struct.pack(">q",mess))
            client.send(bytes(str(mess),'utf-8')) #работает в UTF-8
            #client.send(b'coll') #работает в UTF-8
        else:
            print("Server lost")
            break
        mess+=1
        if (mess==100):
            mess = 1