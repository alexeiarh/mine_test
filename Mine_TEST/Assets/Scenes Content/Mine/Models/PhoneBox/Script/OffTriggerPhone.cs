﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OffTriggerPhone : MonoBehaviour
{
    public GameObject Phone;
    public Rigidbody br;
    public GameObject TriggerPhone;
    void Start()
    {
        br = GetComponent<Rigidbody>();
    }
    private void OnTriggerEnter(Collider PhoneCol)
    {
        if (gameObject.name == "Phone")
        {
            br.isKinematic = true;
            br.constraints = RigidbodyConstraints.FreezePosition;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (gameObject.name == "Phone")
        {
            br.isKinematic = false;
            br.constraints = RigidbodyConstraints.None;
        }
    }
}
