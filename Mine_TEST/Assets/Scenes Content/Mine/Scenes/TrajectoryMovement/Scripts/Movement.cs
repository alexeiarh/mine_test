﻿using PathCreation;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public PathCreator pathCreator;
    public GameObject Server;
    //speed - значение скорости, получаемое из модуля Python на дорожке
    public int speed;
    //speedController - корректирует скорость перемещения
    [SerializeField] private int speedController = 50;
    //distanceTravelled - задает текущую позицию на объекте Path
    [SerializeField] private float distanceTravelled;

    void Update()
    {
        speed = -Server.GetComponent<Netmanager_j>().GetData();
        if (speed < 0)
            speed = 0;
        if (speed > 255)
            speed = 255;
        MovePlayer();
    }

    private void MovePlayer()
    {
        distanceTravelled += speed * (float)(1.0 / speedController) * Time.deltaTime;
        transform.position = pathCreator.path.GetPointAtDistance(distanceTravelled, EndOfPathInstruction.Stop);
        transform.rotation = pathCreator.path.GetRotationAtDistance(distanceTravelled, EndOfPathInstruction.Stop);
    }

    /// <summary>
    /// Метод, в котором задается скорость Player
    /// </summary>
    /// <param name="speed"></param>
    public void SetSpeed(int speed)
    {
        Debug.Log("speed: " + speed);
        this.speed = -speed;
    }
}
