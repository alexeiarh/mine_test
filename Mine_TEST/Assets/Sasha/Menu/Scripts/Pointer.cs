﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Светить этот скрипт в сцене For_Video
/// </summary>
public class Pointer : MonoBehaviour
{
    public Image PointerImage;
    public Light PointLight;

    public float Timer;
    public float MaxTime = 1.0f;

    public bool gazedAt = false;
    public bool enable = true;

    public float LightIntensity = 0.5f;
    public float DefaultLength = 100f;

    public Camera Sourse;
    private Transform sourseTransform;

    private GameObject _prevObject;


    public void Start()
    {
        sourseTransform = Sourse.GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * RayLength, Color.yellow);  
        if (!gazedAt)
        {
            ResetPointer();
        }
        if (enable)
        {
            if (Physics.Raycast(CurrentRay(), out RaycastHit hit, DefaultLength))
            {
                GameObject currObject = hit.collider.gameObject;

                if (currObject != _prevObject || !currObject.CompareTag("Button") || hit.collider == null)
                {
                    gazedAt = false;
                }
                if (currObject.CompareTag("Button"))
                {
                    gazedAt = true;
                }


                if (gazedAt)
                {
                    Timer += Time.deltaTime;
                    PointerImage.fillAmount += Time.deltaTime;
                    PointLight.intensity += Time.deltaTime * LightIntensity;

                    if (Timer >= MaxTime)
                    {
                        hit.collider.gameObject.GetComponent<Button>().onClick.Invoke();

                        ResetPointer(); gazedAt = false;
                    }
                }
                if (!gazedAt)
                {
                    ResetPointer();
                }
                _prevObject = currObject;
            }
        }
       
    }

    private Ray CurrentRay()
    {
        sourseTransform = Sourse.GetComponent<Transform>();
        return new Ray(sourseTransform.position, sourseTransform.forward);
    }

    private void ResetPointer()
    {
        Timer = 0;
        PointerImage.fillAmount = 0;
        PointLight.intensity = 0;
    }
}
