﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchCameras : MonoBehaviour
{
    public GameObject Player;
    public InputManager InputManager;
    public Pointer Pointer;
    [HideInInspector] public Canvas[] canvases;
    [HideInInspector] public Camera currentCamera;
    GameObject SteamVRObject;
    GameObject NoSteamVRFallbackObject;

    void Awake()
    {
        UpdateCameras();

        canvases = FindObjectsOfType<Canvas>();

        CheckCamera();
    }

    private void Start()
    {
        canvases = FindObjectsOfType<Canvas>();
    }

    void Update()
    {
        canvases = FindObjectsOfType<Canvas>();
        CheckCamera();
    }

    private void UpdateCameras()
    {
        try
        {
            SteamVRObject = GameObject.Find("Player").transform.Find("SteamVRObjects").gameObject;
            NoSteamVRFallbackObject = GameObject.Find("Player").transform.Find("NoSteamVRFallbackObjects").gameObject;
        }
        catch (NullReferenceException e)
        {
            Instantiate(Player);
            UpdateCameras();
        }
    }

    private void CheckCamera()
    {
        if (InputManager.IsInVR)
        {
            Debug.Log("In Vr");
            SetVRMode();
        }
        else
        {
            Debug.Log("Not In Vr");
            SetDebagMode();
        }

        Pointer.Sourse = currentCamera;
        foreach (var canvas in canvases)
        {
            canvas.worldCamera = currentCamera;
        }
    }

    private void SetVRMode()
    {
        if (NoSteamVRFallbackObject == null || SteamVRObject == null)
        {
            UpdateCameras();
        }
        currentCamera = SteamVRObject.transform.Find("VRCamera").GetComponent<Camera>();

        SteamVRObject.SetActive(true);
        NoSteamVRFallbackObject.SetActive(false);
    }

    private void SetDebagMode()
    {
        if (NoSteamVRFallbackObject == null || SteamVRObject == null)
        {
            UpdateCameras();
        }
        currentCamera = NoSteamVRFallbackObject.transform.Find("FallbackObjects").GetComponent<Camera>();

        SteamVRObject.SetActive(false);
        NoSteamVRFallbackObject.SetActive(true);
    }
}
