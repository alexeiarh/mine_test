﻿using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class PlayerController : MonoBehaviour
{
    public SteamVR_Action_Vector2 touchpadInput;
    public Transform cameraTransform;
    private CapsuleCollider _capsuleCollider;
    public float speed = 2.0f;

    [HideInInspector] public bool PlayerIsFallingDown = false;

    void Start()
    {
        _capsuleCollider = gameObject.transform.GetChild(0).GetChild(0).GetComponent<CapsuleCollider>();
    }

    void FixedUpdate()
    {
        if (!PlayerIsFallingDown)
        {
            ExecuteMovement();
        }   
    }

    public void ExecuteMovement()
    {
        Vector3 movementDir = Player.instance.hmdTransform.TransformDirection(new Vector3(touchpadInput.axis.x, 0, touchpadInput.axis.y));
        transform.position += Vector3.ProjectOnPlane(Time.deltaTime * movementDir * speed, Vector3.up);

        float distanceFromFloor = Vector3.Dot(cameraTransform.localPosition, Vector3.up);
        _capsuleCollider.height = Mathf.Max(_capsuleCollider.radius, distanceFromFloor);

        _capsuleCollider.center = cameraTransform.localPosition - 0.5f * distanceFromFloor * Vector3.up;
    }
}
